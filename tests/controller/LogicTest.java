package controller;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import models.Polynomial;

public class LogicTest {
	
	Logic logic = new Logic();
	
	Polynomial p1 = new Polynomial("x^2+x+1");
    Polynomial p2 = new Polynomial("x+1");
    Polynomial result;

	@Test
	public void testAdd() {
        result = logic.add(p1, p2);
        Polynomial r = new Polynomial("x^2+2x+2");
        assertEquals(r.toString(), result.toString());
	}

	@Test
	public void testMultiply() {
        result = logic.multiply(p1, p2);
        Polynomial r = new Polynomial("x^3+2x^2+2x+1");
        assertEquals(r.toString(), result.toString());
	}

	@Test
	public void testDivision() {
		List<Polynomial> divisionResults = new ArrayList<Polynomial>();
        divisionResults = logic.division(p1, p2);

        Polynomial quotient = new Polynomial("1x");
        Polynomial remainder = new Polynomial("1");

        assertEquals(quotient.toString(), divisionResults.get(0).toString());
        assertEquals(remainder.toString(), divisionResults.get(1).toString());
	}

	@Test
	public void testDerivative() {
        result = logic.add(p1, p2);
        Polynomial r = new Polynomial("x^2+2x+2");
        assertEquals(r.toString(), result.toString());
	}

}
