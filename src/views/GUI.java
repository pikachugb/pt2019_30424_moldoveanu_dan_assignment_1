package views;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.Logic;
import models.Polynomial;

public class GUI extends JPanel implements ActionListener{

	public static final long serialVersionUID = 1L;
	private static JButton adr, sub, mul, div, itg, der;
	private static GridBagConstraints gbc = new GridBagConstraints();
	private static JTextField op1,op2,op3;
	private static JLabel resLabel1, res1, resLabel2, res2;
	private static Logic logic = new Logic();
	
	public GUI() {
		setLayout(new GridBagLayout());
		
		op1 = new JTextField();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 7;
		gbc.fill = GridBagConstraints.BOTH;
		add(op1,gbc);
		
		op2 = new JTextField();
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 7;
		gbc.fill = GridBagConstraints.BOTH;
		add(op2,gbc);
		
		resLabel1 = new JLabel("RESULT:");
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 7;
		gbc.fill = GridBagConstraints.BOTH;
		add(resLabel1,gbc);
		
		res1 = new JLabel("0");
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 7;
		gbc.fill = GridBagConstraints.BOTH;
		add(res1,gbc);
		
		op3 = new JTextField();
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridwidth = 7;
		gbc.fill = GridBagConstraints.BOTH;
		add(op3,gbc);
		
		resLabel2 = new JLabel("RESULT:");
		gbc.gridx = 0;
		gbc.gridy = 7;
		gbc.gridwidth = 7;
		gbc.fill = GridBagConstraints.BOTH;
		add(resLabel2,gbc);
		
		res2 = new JLabel("0");
		gbc.gridx = 0;
		gbc.gridy = 8;
		gbc.gridwidth = 7;
		gbc.fill = GridBagConstraints.BOTH;
		add(res2,gbc);
		
		adr = new JButton("+");
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 1;
		adr.addActionListener(this);
		add(adr, gbc);

		sub = new JButton("-");
		gbc.gridx = 2;
		gbc.gridy = 2;
		sub.addActionListener(this);
		add(sub, gbc);

		mul = new JButton("*");
		gbc.gridx = 3;
		gbc.gridy = 2;
		mul.addActionListener(this);
		add(mul, gbc);

		div = new JButton("/");
		gbc.gridx = 4;
		gbc.gridy = 2;
		div.addActionListener(this);
		add(div, gbc);

		itg = new JButton("Integrate");
		gbc.gridx = 2;
		gbc.gridy = 6;
		itg.addActionListener(this);
		add(itg, gbc);

		der = new JButton("Derivative");
		gbc.gridx = 3;
		gbc.gridy = 6;
		der.addActionListener(this);
		add(der, gbc);
	}
	
	public void actionPerformed(ActionEvent e) {
		JButton sourceObject = (JButton) e.getSource();
		String operation = sourceObject.getText();
		
		Polynomial first = new Polynomial(op1.getText());
		Polynomial second = new Polynomial(op2.getText());
		Polynomial third = new Polynomial(op3.getText());
		Polynomial result;
		
		switch(operation) {
			case "+": 
				result = logic.add(first,second);
				res1.setText(result.toString()); 
			break;
			
			case "-": 
				result = logic.substract(first,second);
				res1.setText(result.toString());  
			break;
			
			case "*": 
				result = logic.multiply(first, second);
				if ( result != null )
					res1.setText(result.toString()); 
				else 
					res1.setText("0");
			break;
			
			case "/": 
				if (second.toString() != "0") { 
					List<Polynomial> results;
					results = logic.division(first, second);
					res1.setText(results.get(0).toString() + "\n R: " + results.get(1).toString()); 
				}
				else
					res1.setText("Division by 0");
			break;
			
			case "Integrate": 
				result = logic.integrate(third);
				if ( result != null )
					res2.setText(result.toString()); 
				else 
					res2.setText("0");
			break;	
			
			case "Derivative": 
				result = logic.derivative(third);
				if ( result != null )
					res2.setText(result.toString()); 
				else 
					res2.setText("0");
			break;
			default: System.out.println("nopz");
		}
		
	}
	
	public static void main(String args[]) {
		GUI gui = new GUI();
		JFrame frame = new JFrame();
		
		frame.setTitle("Polynomial Operations");
		frame.setSize(340,340);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.add(gui);
	}
}
