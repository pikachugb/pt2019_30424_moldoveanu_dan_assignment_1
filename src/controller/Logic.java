package controller;

import java.util.ArrayList;
import java.util.List;

import models.Monomial;
import models.Polynomial;

public class Logic {
	
	public Polynomial add(Polynomial a, Polynomial b) {
		Polynomial result = a;
		
		if (a == null) 
			return b;
		if (b == null)
			return a;
		
		for( Monomial bMonomial: b.getMonomials()) { 
			double currentCoefficient = bMonomial.getCoefficient();
			int currentExponent = bMonomial.getExponent();
			Boolean found = false;
			
			for (Monomial aMonomial: a.getMonomials()) {
				if ( aMonomial.getExponent() == currentExponent ) {
					found = true;
					aMonomial.setCoefficient(aMonomial.getCoefficient() + currentCoefficient);
				}					
			}
			if (!found)
				result.getMonomials().add(new Monomial(currentCoefficient, currentExponent));
		}
		
		result.deleteZeroCoefficient();		
		result.getMonomials().sort(Monomial.ExponentComparator);
		return result;
	}
	
	public Polynomial substract(Polynomial a, Polynomial b) {
		return add(a, multiply(b,new Polynomial("-1")));
	}
	
	private Monomial multiplyMonomial(Monomial a, Monomial b) {
		return new Monomial(a.getCoefficient() * b.getCoefficient(), a.getExponent() + b.getExponent());
	}
	
	public Polynomial multiply(Polynomial a, Polynomial b) { 
		Polynomial result = null;
		
		if (a == null || b == null) {
			return result;
		}
		
		for ( Monomial aMonomial: a.getMonomials() ) {
			for ( Monomial bMonomial: b.getMonomials() ) {
				Polynomial newMonomial = new Polynomial(multiplyMonomial(aMonomial, bMonomial).toString());
				if (result == null)
					result = newMonomial;
				else {
					double currentCoefficient = multiplyMonomial(aMonomial, bMonomial).getCoefficient();
					int currentExponent = multiplyMonomial(aMonomial, bMonomial).getExponent();
					Boolean found = false;
					
					for ( Monomial resultMonomial: result.getMonomials() ) {
						if ( resultMonomial.getExponent() == currentExponent ) {
							found = true;
							resultMonomial.setCoefficient(resultMonomial.getCoefficient() + currentCoefficient);
						}					
					}
					if (!found)
						result.getMonomials().add(new Monomial(currentCoefficient, currentExponent));
				}
			}
		}
		
		if ( result != null )
			result.getMonomials().sort(Monomial.ExponentComparator);	
		return result;
	}
	
	private Monomial divisionMonomial(Monomial a, Monomial b) {
		return new Monomial(a.getCoefficient() / b.getCoefficient(), a.getExponent() - b.getExponent());
	}
	
	public List<Polynomial> division(Polynomial a, Polynomial b) {
		List<Polynomial> result = new ArrayList<Polynomial>();
		
		Monomial firsta = a.getMonomials().get(0);
		Monomial firstb = b.getMonomials().get(0);
		
		while ( firsta.getExponent() >= firstb.getExponent() ) {
			Monomial newMonomial = divisionMonomial(firsta, firstb);
			if (result.size() == 0)
				result.add(new Polynomial(newMonomial.toString()));
			else {
				Boolean found = false;
				for (Monomial m: result.get(0).getMonomials()) {
					if ( m.getExponent() == newMonomial.getExponent() ) {
						found = true;
						m.setCoefficient(m.getCoefficient() + newMonomial.getCoefficient());
					}
				}
				if (!found)
					result.get(0).getMonomials().add(newMonomial);
			}
			a = substract(a, multiply(b, new Polynomial(newMonomial.toString())));
			a.deleteZeroCoefficient();
			if (a.getMonomials().size() != 0)
				firsta = a.getMonomials().get(0);
			else
				break;
		}
		
		result.add(a);
		
		return result;
	}
	
	public Polynomial derivative(Polynomial a) {
		Polynomial result = null;
		
		for ( Monomial aMonomial: a.getMonomials() ) {
			if (aMonomial.getExponent() != 0) {
				Monomial newMonomial = new Monomial(aMonomial.getCoefficient() * aMonomial.getExponent(), aMonomial.getExponent() - 1);
				if (result == null )
					result = new Polynomial(newMonomial.toString());
				else {
					result.getMonomials().add(newMonomial);
				}
			}
		}
		return result;
	}
	
	public Polynomial integrate(Polynomial a) {
		Polynomial result = null;
		
		for ( Monomial aMonomial: a.getMonomials() ) {
			if (aMonomial.getExponent() != 0) {
				Monomial newMonomial = new Monomial(aMonomial.getCoefficient() / ( aMonomial.getExponent() + 1 ), aMonomial.getExponent() + 1);
				if (result == null )
					result = new Polynomial(newMonomial.toString());
				else {
					result.getMonomials().add(newMonomial);
				}	
			}
		}
		return result;
	}
}
