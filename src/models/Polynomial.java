package models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Polynomial {
	private List<Monomial> monomials = new ArrayList<Monomial>();
	
	public Polynomial(String string) {
		string = string.replace(" ","");
		String[] monStrings = string.split("(?=-)|(?=\\+)");
		for (String current: monStrings) {
			
			if (current == "") 
				break;
			
			if(!current.contains("x"))
				current = current + "x^0";  
			else if (!current.contains("^"))
				current = current + "^1";  
			
			String[] eachMon = new String[2];
			eachMon = current.split("\\^");
			if (eachMon[0].equals("+x") || eachMon[0].equals("-x") || eachMon[0].equals("x"))
				eachMon[0] = eachMon[0].replace("x", "1");
			eachMon[0] = eachMon[0].replace("x", "");
			
			double currentCoefficient = Double.valueOf(eachMon[0]);
			if (currentCoefficient != 0) {
				int currentExponent = Integer.parseInt(eachMon[1]);
				Boolean found = false;
				
				for (Monomial monomial:monomials) {
					if ( monomial.getExponent() == currentExponent ) {
						found = true;
						monomial.setCoefficient(monomial.getCoefficient() + currentCoefficient);
					}					
				}
				if (!found)
					monomials.add(new Monomial(currentCoefficient, currentExponent));
			}
		}
		monomials.sort(Monomial.ExponentComparator);
		deleteZeroCoefficient();
	}
	
	public void deleteZeroCoefficient() {
		List<Monomial> found = new ArrayList<Monomial>();
		for ( Monomial m: monomials ) {
			if ( m.getCoefficient() == 0 ) {
				found.add(m);
			}
		}
		monomials.removeAll(found);
	}
	
	public List<Monomial> getMonomials() {
		return monomials;
	}

	public void setMonomials(List<Monomial> monomials) {
		this.monomials = monomials;
	}
	
	@Override
	public String toString() {
		String aux = new String();
		if ( this != null)
			if ( monomials.size() != 0 )
				for (Monomial mon: monomials) {
					if (mon.getCoefficient() > 0)
						aux += "+";
					aux += mon.toString();
				}
			else
				aux = "0";
		return aux;
	}
	
	public boolean equals(Polynomial p) {
		Iterator<Monomial> i1 = monomials.iterator();
		Iterator<Monomial> i2 = p.getMonomials().iterator();
		
		while (i1.hasNext() || i2.hasNext()) {
			Monomial m1 = (Monomial)i1.next();
			Monomial m2 = (Monomial)i2.next();
			
			if (m1.getExponent() != m2.getExponent())
				return false;
			else if ( m1.getCoefficient() != m2.getCoefficient() )
				return false;
			
			if ((i1.hasNext() && !i2.hasNext())||(!i1.hasNext() && i2.hasNext()))
				return false;
		}
		
		return true;
	}
}
