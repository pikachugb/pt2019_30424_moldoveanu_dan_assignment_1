package models;

import java.util.Comparator;

public class Monomial {
	private double coefficient;
	private int exponent;
	
	public Monomial(double coefficient, int exponent) {
		this.coefficient = coefficient;
		this.exponent = exponent;
	}

	public double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(double coefficient) {
		this.coefficient = coefficient;
	}

	public int getExponent() {
		return exponent;
	}

	public void setExponent(int exponent) {
		this.exponent = exponent;
	}
	
	@Override
	public String toString() {
		if ( exponent == 0 )
			return coefficient+"";
		else if ( exponent == 1 )
			return coefficient+"x";
		else if (coefficient == 1)
			return "x^"+exponent;
		return coefficient+"x^"+exponent;
	}
	
	public static Comparator<Monomial> ExponentComparator = new Comparator<Monomial>() {
		public int compare(Monomial m1, Monomial m2) {
			Integer exponent1 = m1.getExponent();
			Integer exponent2 = m2.getExponent();
			return exponent2.compareTo(exponent1);
		}
	};
}
